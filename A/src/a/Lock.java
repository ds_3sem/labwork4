package a;

public class Lock {
    private int readers = 0;
    private boolean isWriting = false;

    public synchronized void readLock() throws InterruptedException {
        while (isWriting) {
            wait();
        }
        readers++;
    }

    public synchronized void readUnlock() {
        if (readers == 0) {
            throw new IllegalMonitorStateException();
        }
        readers--;
        if (readers == 0) {
            notifyAll();
        }
    }

    public synchronized void writeLock() throws InterruptedException {
        while (readers != 0) {
            wait();
        }
        isWriting = true;
    }

    public synchronized void writeUnlock() {
        if (!isWriting) {
            throw new IllegalMonitorStateException();
        }
        isWriting = false;
        notifyAll();
    }
}
