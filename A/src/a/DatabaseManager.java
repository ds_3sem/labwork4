package a;

import static a.Main.lock;
import static a.Main.db;

import java.io.IOException;
import java.util.Random;

public class DatabaseManager extends Thread {
    private final String[] records;
    private boolean empty = false;
    private int nextToRemove = -1;
    private int nextToAdd = 0;

    public DatabaseManager(String[] records) {
        this.records = records;
    }

    @Override
    public void run() {
        Random rand = new Random();
        while (!empty) {
            try {
                lock.writeLock();
                int operation = rand.nextInt(2);
                if (operation == 0 && nextToRemove != -1) {
                    String toRemove = records[nextToRemove];
                    db.removeRecord(new Record(toRemove));
                    System.out.println("\nRecord " + toRemove + " was removed");
                    nextToRemove = nextToAdd - 1;
                } else if (operation == 1) {
                    String toAdd = records[nextToAdd];
                    db.addRecord(new Record(toAdd));
                    System.out.println("\nRecord " + toAdd + " was added");
                    nextToAdd++;
                    if (nextToAdd == records.length) {
                        empty = true;
                    }
                }
            } catch (InterruptedException | IOException e) {
                throw new RuntimeException(e);
            } finally {
                lock.writeUnlock();
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
