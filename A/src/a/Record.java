package a;

public class Record {
    private final String name;
    private final String lastName;
    private final String number;

    public Record(String line) {
        var record = line.split(" : ");
        this.name = record[0];
        var fullname = name.split(" ");
        this.lastName = fullname[1];
        this.number = record[1];
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNumber() {
        return number;
    }

    public String getLine() {
        return String.format("%s : %s", name, number);
    }
}
