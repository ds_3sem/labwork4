package a;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Database {
    private final Path path;

    public Database(String filename) {
        this.path = Paths.get(filename);
    }

    public void addRecord(Record record) throws IOException {
        String toAdd = record.getLine();
        toAdd = toAdd + "\n";
        byte[] bytes = toAdd.getBytes();
        Files.write(path, bytes, StandardOpenOption.APPEND);
    }

    public void removeRecord(Record record) throws IOException {
        String toRemove = record.getLine();
        List<String> lines = Files.readAllLines(path);
        clearFile();
        for (String line : lines) {
            if (!toRemove.equals(line)) {
                line += "\n";
                Files.write(path, line.getBytes(), StandardOpenOption.APPEND);
            }
        }
    }

    public List<String> searchByName(String lastName) throws IOException {
        List<String> numbers = new ArrayList<String>();
        List<String> lines = Files.readAllLines(path);
        for (String line : lines) {
            Record record = new Record(line);
            if (Objects.equals(lastName, record.getLastName())) {
                numbers.add(record.getNumber());
            }
        }
        return numbers;
    }

    public String searchByNumber(String number) throws IOException {
        String name = "";
        List<String> lines = Files.readAllLines(path);
        for (String line : lines) {
            Record record = new Record(line);
            if (Objects.equals(number, record.getNumber())) {
                return record.getName();
            }
        }
        return name;
    }

    public void clearFile() throws IOException {
        byte[] empty = "".getBytes();
        Files.write(path, empty);
    }
}
