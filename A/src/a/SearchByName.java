package a;

import java.io.IOException;
import java.util.List;

import static a.Main.lock;
import static a.Main.db;

public class SearchByName extends Thread {
    private final String[] lastNames;

    public SearchByName(String[] lastNames) {
        this.lastNames = lastNames;
    }

    @Override
    public void run() {
        for (String lastName : lastNames) {
            try {
                lock.readLock();
                List<String> numbers = db.searchByName(lastName);
                if (!numbers.isEmpty()) {
                    System.out.printf("\nFound number(s) for %s is/are ", lastName);
                    for (String number : numbers) {
                        System.out.printf("%s ", number);
                    }
                } else {
                    System.out.printf("\n%s is not in the database", lastName);
                }
            } catch (InterruptedException | IOException e) {
                throw new RuntimeException(e);
            } finally {
                lock.readUnlock();
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
