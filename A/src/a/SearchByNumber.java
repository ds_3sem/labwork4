package a;

import java.io.IOException;
import java.util.Objects;

import static a.Main.lock;
import static a.Main.db;

public class SearchByNumber extends Thread {
    private final String[] numbers;

    public SearchByNumber(String[] numbers) {
        this.numbers = numbers;
    }

    @Override
    public void run() {
        for (String number : numbers) {
            try {
                lock.readLock();
                String name = db.searchByNumber(number);
                if (!Objects.equals(name, "")) {
                    System.out.printf("\nNumber %s is registered with %s", number, name);
                } else {
                    System.out.printf("\nNumber %s belongs to no one", number);
                }
            } catch (InterruptedException | IOException e) {
                throw new RuntimeException(e);
            } finally {
                lock.readUnlock();
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
