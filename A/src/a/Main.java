package a;

import java.io.IOException;

public class Main {
    static final Lock lock = new Lock();
    static final Database db = new Database("text.txt");

    public static void main(String[] args) throws IOException, InterruptedException {
        String[] records = {"Sienna Grey : 44758204983", "Alyssa Addams : 38093285793", "Wyatt Grey : 1023462375", "Dawn Summers : 37098326357", "Earl Grey : 4467298756723", "Marian Blossom : 556287836474", "Anabella Stuart : 2884209374784", "Nathaniel Addams : 3890237854578"};
        String[] names = {"Grey", "Addams", "Blossom"};
        String[] numbers = {"44758204983", "37098326357", "556287836474"};

        SearchByName thread1 = new SearchByName(names);
        SearchByNumber thread2 = new SearchByNumber(numbers);
        DatabaseManager thread3 = new DatabaseManager(records);

        thread3.start();
        thread1.start();
        thread2.start();

        thread3.join();
        thread1.join();
        thread2.join();
    }
}