package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

var (
	lock   sync.RWMutex
	cities []bool
	cost   [][]int
)

func priceUpdate() {
	for {
		lock.Lock()

		rand1 := rand.Intn(len(cities))

		if cities[rand1] {
			rand2 := rand.Intn(len(cities))

			if cities[rand2] && rand1 != rand2 && cost[rand1][rand2] > 0 {
				newPrice := 1 + rand.Intn(50)
				cost[rand1][rand2] = newPrice
				cost[rand2][rand1] = newPrice

				fmt.Printf("Price for route %v <--> %v is %v now\n", rand1, rand2, newPrice)
			}
		}
		lock.Unlock()
		time.Sleep(time.Duration(rand.Intn(3)+3) * time.Second)
	}
}

func routesManager() {
	for {
		lock.Lock()

		rand1 := rand.Intn(len(cities))

		if cities[rand1] {
			rand2 := rand.Intn(len(cities))

			if cities[rand2] && rand1 != rand2 {
				if cost[rand1][rand2] == 0 {
					price := 1 + rand.Intn(50)
					cost[rand1][rand2] = price
					cost[rand2][rand1] = price

					fmt.Printf("NEW route %v <--> %v was added. Price is %v\n", rand1, rand2, price)
				} else {
					cost[rand1][rand2] = 0
					cost[rand2][rand1] = 0

					fmt.Printf("Route %v <--> %v is not available anymore\n", rand1, rand2)
				}
			}
		}
		lock.Unlock()
		time.Sleep(time.Duration(rand.Intn(3)+3) * time.Second)
	}
}

func citiesManager() {
	for {
		lock.Lock()

		rand1 := rand.Intn(len(cities))

		if cities[rand1] {
			cities[rand1] = false

			fmt.Printf("City %v was deleted\n", rand1)

			for i := 0; i < len(cities); i++ {
				if cost[rand1][i] > 0 {
					cost[rand1][i] = 0
					cost[i][rand1] = 0

					fmt.Printf("Route %v <--> %v is not available anymore\n", rand1, i)
				}
			}
		} else {
			cities[rand1] = true

			fmt.Printf("City %v was added\n", rand1)
		}
		lock.Unlock()
		time.Sleep(time.Duration(rand.Intn(3)+3) * time.Second)
	}
}

func routePrice() {
	for {
		lock.RLock()

		rand1 := rand.Intn(len(cities))

		if cities[rand1] {
			rand2 := rand.Intn(len(cities))

			if cities[rand2] && rand1 != rand2 {
				if cost[rand1][rand2] > 0 {
					fmt.Printf("Price for DIRECT route %v <--> %v is %v\n", rand1, rand2, cost[rand1][rand2])
				} else {
					fmt.Printf("Price for route %v <--> %v is ", rand1, rand2)
					price := routeFinder(rand1, rand2, -1)
					if price > 0 {
						fmt.Printf("%v\n", price)
					} else {
						fmt.Printf("not found\n")
					}
				}
			}
		}
		lock.RUnlock()
		time.Sleep(time.Duration(rand.Intn(3)+3) * time.Second)
	}
}

func routeFinder(a int, b int, prev int) int {
	for i := prev + 1; i < len(cities); i++ {
		if i == a {
			continue
		}
		if i == len(cities) {
			return 0
		}

		price := cost[a][i]

		if i == b && price > 0 {
			return price
		}

		if price > 0 {
			p := routeFinder(i, b, a)
			if p == 0 {
				continue
			}
			price = price + p
			return price
		}
	}
	return 0
}

func await() {
	exitSignal := make(chan os.Signal)
	signal.Notify(exitSignal, syscall.SIGINT, syscall.SIGTERM)
	<-exitSignal
}

func main() {
	cities = []bool{true, true, true, true, true}
	cost = [][]int{
		{0, 15, 10, 0, 0},
		{12, 0, 0, 0, 0},
		{31, 0, 0, 41, 0},
		{0, 0, 23, 0, 33},
		{0, 0, 0, 16, 0},
	}
	go priceUpdate()
	go routesManager()
	go citiesManager()
	go routePrice()

	await()
}
