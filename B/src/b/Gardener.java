package b;

import static b.Main.sizeX;
import static b.Main.sizeY;
import static b.Main.writeLock;

public class Gardener extends Thread {
    private final int[][] garden;

    public Gardener(int[][] garden) {
        this.garden = garden;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            writeLock.lock();
            try {
                for (int i = 0; i < sizeX; i++) {
                    for (int j = 0; j < sizeY; j++) {
                        if (garden[i][j] == 0) {
                            garden[i][j] = 1;
                        }
                    }
                }
            } finally {
                writeLock.unlock();
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
