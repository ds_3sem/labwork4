package b;

import static b.Main.sizeX;
import static b.Main.sizeY;
import static b.Main.writeLock;

public class Nature extends Thread {
    private final int[][] garden;

    public Nature(int[][] garden) {
        this.garden = garden;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            writeLock.lock();
            try {
                for (int i = 0; i < 5; i++) {
                    garden[getRandom(sizeX)][getRandom(sizeY)] = 0;
                }
            } finally {
                writeLock.unlock();
            }

            try {
                Thread.sleep(750);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private int getRandom(int size) {
        return (int) (Math.random() * size);
    }
}
