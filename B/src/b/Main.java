package b;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Main {
    static final int sizeX = 5;
    static final int sizeY = 5;
    static final ReadWriteLock lock = new ReentrantReadWriteLock();
    static final Lock readLock = lock.readLock();
    static final Lock writeLock = lock.writeLock();
    static final String filename = "text.txt";

    public static void main(String[] args) throws InterruptedException {
        int[][] garden = new int[sizeX][sizeY];

        Gardener gardener = new Gardener(garden);
        Nature nature = new Nature(garden);
        Monitor1 monitor1 = new Monitor1(garden);
        Monitor2 monitor2 = new Monitor2(garden);

        gardener.start();
        nature.start();
        monitor1.start();
        monitor2.start();

        gardener.join();
        nature.join();
        monitor1.join();
        monitor2.join();
    }
}