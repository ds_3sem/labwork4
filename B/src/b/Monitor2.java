package b;

import static b.Main.readLock;

public class Monitor2 extends Thread {
    private final int[][] garden;

    public Monitor2(int[][] garden) {
        this.garden = garden;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            readLock.lock();
            try {
                StringBuilder sb = Monitor1.getGarden(garden);
                System.out.println(sb);
            } finally {
                readLock.unlock();
            }

            try {
                Thread.sleep(1250);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
