package b;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static b.Main.sizeX;
import static b.Main.sizeY;
import static b.Main.readLock;
import static b.Main.filename;

public class Monitor1 extends Thread {
    private final int[][] garden;
    Path path = Paths.get(filename);

    public Monitor1(int[][] garden) {
        this.garden = garden;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            readLock.lock();
            try {
                StringBuilder sb = getGarden(garden);
                sb.append('\n');
                Files.write(path, sb.toString().getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                readLock.unlock();
            }

            try {
                Thread.sleep(1250);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    static StringBuilder getGarden(int[][] garden) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                sb.append(garden[i][j]);
                sb.append(' ');
            }
            sb.append('\n');
        }
        return sb;
    }
}
